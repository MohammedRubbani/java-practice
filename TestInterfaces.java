public class TestInterfaces {
    
    public static void main(String[] args) {
        
        Detailable[] products = {
            new CurrentAccount("Current", 1000),
            new SavingsAccount("Savings", 250),
            new HomeInsurance(200, 250, 1000)
        };

        for (Detailable prod: products) {
            System.out.println(prod.getDetails());
        }

    }
}