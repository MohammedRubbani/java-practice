package com.conygre.exception_handling;

public class TestExceptions {
    
    public static void main(String[] args) {

        Account[] accounts = new Account[3];

        try {

            accounts[0] = new SavingsAccount("Fingers", 2);
            accounts[1] = new SavingsAccount("Saving2", 4);
            accounts[2] = new CurrentAccount("Current", 6);

        } catch (DodgyNameException e)
        {
            System.out.println("Exception: " + e.toString());
            return;

        } finally {

            double taxCollected = 0;

            for (Account acc : accounts) {
                taxCollected += acc.getBalance() * 40 / 100;
                acc.setBalance(acc.getBalance() * 60 / 100);
                
            }
            System.out.println(taxCollected);
        }

        
    }
}