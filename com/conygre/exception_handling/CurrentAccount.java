package com.conygre.exception_handling;

public class CurrentAccount extends Account {

    public CurrentAccount(String name, double balance) throws DodgyNameException{
        super(name, balance);
    }

    public void addInterest() {
        setBalance(getBalance() * 1.1);
    }

}