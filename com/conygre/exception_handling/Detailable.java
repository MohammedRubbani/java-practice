package com.conygre.exception_handling;

public interface Detailable {
    String getDetails();
}