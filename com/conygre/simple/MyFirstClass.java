package com.conygre.simple;

public class MyFirstClass {
    
    public static void main(String[] args) {
        
        String make = "BMW";
        String model = "X5";
        double engineSize = 3;
        byte gear = 5;
        short speed = (short)(gear * 10);

        System.out.println("The make is " + make);
        System.out.println("The engine size is " + engineSize);
        System.out.println("The gear is "+ gear);
        System.out.println("The speed is "+ speed);

        if (engineSize >= 1.8) {
            System.out.println("The car is powerful");
        } else {
            System.out.println("The car is weak");
        }

        // if (gear == 1){
        //     System.out.println("The speed should be less than 10mph");
        // } else if (gear == 2){
        //     System.out.println("The speed should be between 10mph and 20mph");
        // } else if (gear == 3){
        //     System.out.println("The speed should be between 20mph and 35mph");
        // } else if (gear == 4){
        //     System.out.println("The speed should be between 35mph and 50");
        // } else {
        //     System.out.println("The speed should be greater than 50mph");
        // }

        switch(gear){
            case 1:
            
        }

        int count = 0;
        for (int i = 1900; i < 2000; i++) {
            if (count==5){
                System.out.println("Finished!");
                break;
            }
            if((i%4)== 0){
                count++;
                System.out.println(i +" is a leapyear");
            }
            
        }

    }
}
