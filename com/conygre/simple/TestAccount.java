package com.conygre.simple;

public class TestAccount {
    public static void main(String[] args) {


        //creating a null referenced array
        Account arrayOfAccounts[] = new Account[5]; 

        // using static method
        Account.setInterestRate(1.1);

        double[] amounts = {20, 500, 2000, 5, 140};
        String[] names = {"Mo", "John", "Alex", "Alice", "Sue"};

        for (int i = 0; i < arrayOfAccounts.length; i++) {

            // populating the array with objects
            //arrayOfAccounts[i] = new Account(names[i], amounts[i]);

            System.out.println(arrayOfAccounts[i].getBalance());
            
            // arrayOfAccounts[i].setName(names[i]);
            // arrayOfAccounts[i].setBalance(amounts[i]);
            arrayOfAccounts[i].addInterest();

            System.out.println(arrayOfAccounts[i].getName());
            System.out.println(arrayOfAccounts[i].getBalance());


        }

        System.out.println(arrayOfAccounts[1].withdraw(1000));
        System.out.println(arrayOfAccounts[1].getBalance());


    }
}