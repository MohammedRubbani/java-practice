package com.conygre.simple;

public interface Detailable {
    String getDetails();
}