package com.conygre.simple;

public abstract class Account implements Detailable {
    private double balance;
    private String name;

    // will be the same value across objects of the same class
    private static double interestRate;

    // constructor
    public Account(String name, double balance){
        this.name = name;
        this.balance = balance;
     
    } 

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();   
    

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount) {

        // if withdraw can happen
        if (amount < getBalance()){

            double newBalance = getBalance() - amount;
            setBalance(newBalance);
            System.out.println("Withdraw successful");

            return true;
        }
        System.out.println("Withdraw unsuccessful");

        return false;
    }

    public boolean withdraw() {
        
        
        if (20 < getBalance()){
            System.out.println("Withdraw successful");

            double newBalance = getBalance() - 20;
            setBalance(newBalance);



            return true;
        }
        System.out.println("Withdraw unsuccessful");
        return false;
        
    }

    @Override
    public String getDetails() {
        // TODO Auto-generated method stub
        return  " " + name + " " + balance;
    }

    
}