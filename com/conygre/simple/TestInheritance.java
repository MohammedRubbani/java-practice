package com.conygre.simple;

public class TestInheritance {
    
    public static void main(String[] args) {
        
        Account[] accounts = new Account[3];

        accounts[0] = new SavingsAccount("Savings1", 2);
        accounts[1] = new SavingsAccount("Saving2", 4);
        accounts[2] = new CurrentAccount("Current", 6);

        for (Account acc : accounts) {
            acc.addInterest();
            System.out.println("The account name is " + acc.getName());
            System.out.println("The account balance is " + acc.getBalance());

        }
    }
}