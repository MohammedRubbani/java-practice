package com.conygre.simple;

public class TestStrings {
    public static void main(String[] args) {

        StringBuilder val = new StringBuilder("example.doc");
        val.replace(val.length()-3, val.length(), "bak");
        System.out.println(val);

        String name1 = "John";
        String name2 = "Steve";

        if (name2.equals(name1)){
            System.out.println("These two strings are equal");
        } else {
           String greater = (name1.compareTo(name2) > 0) ? name1 : name2;
           System.out.println(greater);
        }

        



    

    }
    
}