package com.conygre.simple;

import java.util.HashSet;
import java.util.Iterator;


public class CollectionsTest {
    
    public static void main(String[] args) {
        
        HashSet<Account> accounts = new HashSet<Account>();

        Account[] acc = {
            new SavingsAccount("Savings", 1000),
            new SavingsAccount("Savings", 200),
            new CurrentAccount("Current", 20)
        };

        for (int i =0; i <3; i ++){
            accounts.add(acc[i]);
        }

        // Iterator<Account> iter = accounts.iterator();

        // while (iter.hasNext()) {
        //     Account nextAccount = iter.next();
        //     System.out.println(nextAccount.getDetails());
        //     nextAccount.addInterest();
        //     System.out.println(nextAccount.getDetails());

        // }

        // for (Account account: accounts) {
        //     System.out.println(account.getDetails());
        //     account.addInterest();
        //     System.out.println(account.getDetails());
        // } 

        accounts.forEach(account -> {
            System.out.println(account.getDetails());
            account.addInterest();
            System.out.println(account.getDetails());
            }    
        );
        
        
    }
}